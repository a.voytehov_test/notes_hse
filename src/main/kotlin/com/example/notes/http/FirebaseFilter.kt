package com.example.notes.http

import com.example.notes.security.authentications.FirebaseAuthentication
import com.example.notes.security.managers.CustomAuthenticationManager
import com.example.notes.service.UserVerificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.WebApplicationContext
import org.springframework.web.context.support.WebApplicationContextUtils
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
class FirebaseFilter : OncePerRequestFilter() {

    var userVerificationService: UserVerificationService? = null

    @Autowired
    lateinit var customAuthenticationManager: CustomAuthenticationManager

    @ExceptionHandler(IllegalStateException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        //TODO refactor spring security conf
        if (request.requestURL.endsWith("/user/save")) {
            filterChain.doFilter(request, response)
            return
        }
        if (userVerificationService == null) {
            val servletContext = request.servletContext
            val webApplicationContext: WebApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(servletContext)!!
            userVerificationService = webApplicationContext.getBean(UserVerificationService::class.java)
        }
        val token = request.getHeader("Authorization")
        if (token == null) {
            forbiddenResponse(response, "no Authorization header in request")
        }
        val authentication = FirebaseAuthentication(token)
        customAuthenticationManager.authenticate(authentication)
        SecurityContextHolder.getContext().authentication = authentication
        filterChain.doFilter(request, response)
    }

    private fun forbiddenResponse(response: HttpServletResponse, message: String) {
        response.status = HttpStatus.FORBIDDEN.value();
        response.writer.write(message)
    }
}