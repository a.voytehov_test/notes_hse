package com.example.notes.security.authentications

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import kotlin.properties.Delegates

class FirebaseAuthentication(val token: String) : Authentication {
    var userId: Int by Delegates.notNull()

    @set:JvmName("setAuthenticatedField")
    var authenticated: Boolean by Delegates.notNull()

    override fun getName(): String {
        return userId.toString()
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        TODO("Not yet implemented")
    }

    override fun getCredentials(): Any {
        TODO("Not yet implemented")
    }

    override fun getDetails(): Any {
        TODO("Not yet implemented")
    }

    override fun getPrincipal(): Any {
        TODO("Not yet implemented")
    }

    override fun isAuthenticated(): Boolean {
        return authenticated
    }

    override fun setAuthenticated(isAuthenticated: Boolean) {
        authenticated = isAuthenticated
    }
}