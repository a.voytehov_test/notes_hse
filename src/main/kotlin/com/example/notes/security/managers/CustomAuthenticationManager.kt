package com.example.notes.security.managers

import com.example.notes.security.providers.FirebaseAuthenticationProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component

@Component
class CustomAuthenticationManager() : AuthenticationManager {
    @Autowired
    lateinit var firebaseAuthenticationProvider: FirebaseAuthenticationProvider

    override fun authenticate(authentication: Authentication?): Authentication {
        if (firebaseAuthenticationProvider.supports(authentication!!.javaClass)) {
            return firebaseAuthenticationProvider.authenticate(authentication)
        }
        throw BadCredentialsException("")
    }
}