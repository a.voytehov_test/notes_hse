package com.example.notes.security.providers

import FIREBASE_APP
import com.example.notes.repository.UserJPA
import com.example.notes.security.authentications.FirebaseAuthentication
import com.google.firebase.auth.FirebaseAuth
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.lang.RuntimeException

@Component
class FirebaseAuthenticationProvider : AuthenticationProvider {

    @Autowired
    lateinit var userJPA: UserJPA

    override fun authenticate(authentication: Authentication?): Authentication {
        val firebaseAuthentication: FirebaseAuthentication = authentication as FirebaseAuthentication
        val email =
            FirebaseAuth.getInstance(FIREBASE_APP).verifyIdToken(firebaseAuthentication.token).email ?: throw RuntimeException("Can't verify user")
        val userId = userJPA.getByEmail(email).id!!
        firebaseAuthentication.userId = userId
        firebaseAuthentication.authenticated = true
        return firebaseAuthentication;
    }

    override fun supports(authentication: Class<*>?): Boolean {
        return authentication == FirebaseAuthentication::class.java
    }
}