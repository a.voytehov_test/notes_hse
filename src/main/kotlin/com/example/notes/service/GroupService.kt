package com.example.notes.service

import com.example.notes.model.Group
import com.example.notes.repository.GroupJPA
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
class GroupService {

    @Autowired
    lateinit var groupJPA: GroupJPA

    @Autowired
    lateinit var userVerificationService: UserVerificationService

    fun get(groupId: Int): Group {
        return groupJPA.get(groupId, userVerificationService.getCurrentUserId())
    }

    fun getAll(): List<Group> {
        return groupJPA.getAll(userVerificationService.getCurrentUserId())
    }

    @Transactional
    fun save(group: Group): Group {
        return groupJPA.save(group)
    }

    @Transactional
    fun delete(groupId: Int): Boolean {
        return groupJPA.delete(groupId, userVerificationService.getCurrentUserId()) != 0
    }
}