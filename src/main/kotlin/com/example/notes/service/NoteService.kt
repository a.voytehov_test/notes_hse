package com.example.notes.service

import com.example.notes.model.Note
import com.example.notes.repository.NoteJPA
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
class NoteService {

    @Autowired
    lateinit var noteJPA: NoteJPA

    @Autowired
    lateinit var userVerificationService: UserVerificationService

    fun get(noteId: Int): Note {
        return noteJPA.get(noteId, userVerificationService.getCurrentUserId())
    }

    fun getAll(): List<Note> {
        return noteJPA.getAll(userVerificationService.getCurrentUserId())
    }

    fun getAllByGroup(groupId: Int): List<Note> {
        return noteJPA.getAllByGroup(userVerificationService.getCurrentUserId(), groupId)
    }

    fun getByIsDone(isDone: Boolean): List<Note> {
        return noteJPA.getByIsDone(userVerificationService.getCurrentUserId(), isDone)
    }

    fun getOverdue(deadline: LocalDateTime): List<Note> {
        return noteJPA.getOverdue(userVerificationService.getCurrentUserId(), deadline)
    }

    @Transactional
    fun delete(noteId: Int): Boolean {
        return noteJPA.delete(noteId, userVerificationService.getCurrentUserId()) != 0
    }

    @Transactional
    fun save(note: Note): Note {
        return noteJPA.save(note)
    }
}