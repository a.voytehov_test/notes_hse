package com.example.notes.service

import com.example.notes.model.User
import com.example.notes.repository.UserJPA
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.SQLException

@Service
class UserService {
    @Autowired
    lateinit var userJPA: UserJPA

    @Autowired
    lateinit var userVerificationService: UserVerificationService

    fun get(): User {
        return userJPA.get(userVerificationService.getCurrentUserId())
    }

    @Transactional
    fun save(user: User): User {
        try {
            return userJPA.save(user)
        } catch (e: DataIntegrityViolationException) {
            throw RuntimeException(e)
        }

    }

}