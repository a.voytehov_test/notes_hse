package com.example.notes.service

import com.example.notes.security.authentications.FirebaseAuthentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class UserVerificationService {
    fun getCurrentUserId(): Int {
        val authentication = SecurityContextHolder.getContext().getAuthentication()
        if (authentication is FirebaseAuthentication) {
            return authentication.userId
        }
        throw RuntimeException("No user logged in")
    }

}